<?php
// Text
$_['text_items']       = '<span class="lbl block-to-inline">Корзина</span><span class="cart-qty">%s</span><span class="cart-summ">%s</span>';
$_['text_empty']    = 'Ваша корзина пуста!';
$_['text_cart']     = 'Перейти в корзину';
$_['text_checkout'] = 'Оформить заказ';
$_['text_recurring']  = 'Платежный профиль';

