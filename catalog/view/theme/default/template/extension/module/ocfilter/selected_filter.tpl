<?php if ($selecteds) { ?>
<div class="list-group-item selected-options">
  <?php foreach ($selecteds as $option) { ?>
    <?php foreach ($option['values'] as $value) { ?>
    <button type="button" onclick="location = '<?php echo $value['href']; ?>';" class="btn btn-xs btn-danger" style="padding: 1px 4px;"><i class="fa fa-times"></i> <?php echo $value['name']; ?></button>
    <?php } ?>
  <?php } ?>
	<?php $count = count($selecteds); $selected = $selecteds; $first = array_shift($selected); ?>
  <?php if ($count > 1 || count($first['values']) > 1) { ?>
  <button type="button" onclick="location = '<?php echo $link; ?>';" class="btn btn-block btn-danger" style="border-radius: 0;"><i class="fa fa-times-circle"></i> Сбросить</button>
  <?php } ?>
</div>
<?php } ?>