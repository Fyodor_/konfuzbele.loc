<div id="search" class="input-group">
  <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $this->journal2->settings->get('search_placeholder_text'); ?>" autocomplete="off" class="form-control input-lg" />
  <div class="button-search"><button type="button">Найти</button></div>
</div>