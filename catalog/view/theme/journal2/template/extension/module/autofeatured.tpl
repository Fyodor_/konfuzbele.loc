 <div class="box related-products-second journal-carousel">
        <div>
          <div class="box-heading"><?php echo $heading_title; ?></div>
          <div class="box-product box-content">
          <div class="swiper">
			  <div class="swiper-container">
				  <div class="swiper-wrapper">
					<?php foreach ($products as $product) { ?>
					
						<div class="product-grid-item swiper-slide xs-100 sm-50 md-33 lg-25 xl-25 display-both block-button">
						  <div class="product-thumb product-wrapper">
							<div class="image">
							  <a href="<?php echo $product['href']; ?>">
							  <img class="first-image" src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" />
							  </a>
							   <?php if (isset($product['labels']) && is_array($product['labels'])): ?>
								  <?php foreach ($product['labels'] as $label => $name): ?>
										<?php if($label == 'sale'){?>
											<span class="label-<?php echo $label; ?>">Акция</span>
										<?php } else { ?>
											<span class="label-<?php echo $label; ?>"><?php echo $name; ?></span>
										<?php } ?>	
								  <?php endforeach; ?>
							  <?php endif; ?>
							</div>
							<div class="product-details">
							  <div class="caption">
								<h4 class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
								<?php if ($product['price']) { ?>
								<p class="price">
								  <?php if (!$product['special']) { ?>
								  <?php echo $product['price']; ?>
								  <?php } else { ?>
								  <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new" <?php echo isset($product['date_end']) && $product['date_end'] ? "data-end-date='{$product['date_end']}'" : ""; ?>><?php echo $product['special']; ?></span>
								  <?php } ?>
								  <?php if ($product['tax']) { ?>
								  <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
								  <?php } ?>
								</p>
								<?php } ?>
							  </div>
							  <div class="button-group">
								<div class="cart <?php echo isset($product['labels']) && is_array($product['labels']) && isset($product['labels']['outofstock']) ? 'outofstock' : ''; ?>">
								  <a onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button hint--top" data-hint="<?php echo $button_cart; ?>"><i class="button-left-icon"></i><span class="button-cart-text">В корзину</span><i class="button-right-icon"></i></a>
								</div>
								<div class="wishlist"><a onclick="addToWishList('<?php echo $product['product_id']; ?>');" class="hint--top" data-hint="<?php echo $button_wishlist; ?>"><i class="wishlist-icon"></i><span class="button-wishlist-text"><?php echo $button_wishlist;?></span></a></div>
								<div class="compare"><a onclick="addToCompare('<?php echo $product['product_id']; ?>');" class="hint--top" data-hint="<?php echo $button_compare; ?>"><i class="compare-icon"></i><span class="button-compare-text">Сравнить</span></a></div>
							  </div>
							</div>
						  </div>
						</div>
						
					<?php } ?>
				  </div>
			  </div>
			  <div class="swiper-button-next"></div>
			  <div class="swiper-button-prev"></div>
          </div>
          </div>
        </div>
      </div>
<script>
        (function () {
          var grid = $.parseJSON('[[0,1],[470,2],[760,3],[980,4],[1100,4]]');

            var breakpoints = {
            470: {
              slidesPerView: grid[0][1],
              slidesPerGroup: grid[0][1]
            },
            760: {
              slidesPerView: grid[1][1],
              slidesPerGroup: grid[1][1]
            },
            980: {
              slidesPerView: grid[2][1],
              slidesPerGroup: grid[2][1]
            },
            1220: {
              slidesPerView: grid[3][1],
              slidesPerGroup: grid[3][1]
            }
          };

          var opts = {
            slidesPerView: grid[4][1],
            slidesPerGroup: grid[4][1],
            breakpoints: breakpoints,
            spaceBetween: parseInt('<?php echo $this->journal2->settings->get('product_grid_item_spacing', '20'); ?>', 10),
            pagination: <?php echo $this->journal2->settings->get('related_products_carousel_bullets') ? '$(\'.related-products-second .swiper-pagination\')' : 'false'; ?>,
            paginationClickable: true,
            nextButton: <?php echo $this->journal2->settings->get('related_products_carousel_arrows') !== 'none' ? '$(\'.related-products-second .swiper-button-next\')' : 'false'; ?>,
            prevButton: <?php echo $this->journal2->settings->get('related_products_carousel_arrows') !== 'none' ? '$(\'.related-products-second .swiper-button-prev\')' : 'false'; ?>,
            autoplay: <?php echo $this->journal2->settings->get('related_products_carousel_autoplay') > 0 ? 4000 : 'false'; ?>,
            autoplayStopOnHover: <?php echo $this->journal2->settings->get('related_products_carousel_pause_on_hover') ? 'true' : 'false'; ?>,
            speed: 400,
            touchEventsTarget: <?php echo $this->journal2->settings->get('related_products_carousel_touchdrag')  ? '\'container\'' : 'false'; ?>,
          };

          $('.related-products-second .swiper-container').swiper(opts);
        })();
</script>	  
