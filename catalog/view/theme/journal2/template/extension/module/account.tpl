<div class="box account-left-block">
  <div class="box-content list-group">
	  <div class="top-block">
		<?php echo $name; ?>
		<div class="eml"><?php echo $email; ?></div>
	  </div>	
      <ul>
        <?php if (!$logged) { ?>
        <li><a href="<?php echo $login; ?>" class="list-group-item"><?php echo $text_login; ?></a></li>
        <li><a href="<?php echo $register; ?>" class="list-group-item"><?php echo $text_register; ?></a></li>
        <li><a href="<?php echo $forgotten; ?>" class="list-group-item"><?php echo $text_forgotten; ?></a></li>
        <?php } ?>
        <?php if ($logged) {?>
        <li><a href="<?php echo $edit; ?>" class="list-group-item">Личные данные</a></li>
        <?php } ?>
        <li><a href="<?php echo $address; ?>" class="list-group-item">Мои адреса</a></li>
		<li><a href="<?php echo $order; ?>" class="list-group-item"><?php echo $text_order; ?></a></li>
        <li><a href="<?php echo $wishlist; ?>" class="list-group-item">Мои закладки</a></li>
        <li><a href="<?php echo $reward; ?>" class="list-group-item">Мои бонусы</a></li>
        <li><a href="<?php echo $newsletter; ?>" class="list-group-item">Рассылки</a></li>
        <?php if ($logged) { ?>
		<li><a href="<?php echo $password; ?>" class="list-group-item">Изменить пароль</a></li>
        <li><a href="<?php echo $logout; ?>" class="list-group-item"><?php echo $text_logout; ?></a></li>
        <?php } ?>
      </ul>
  </div>
</div>
