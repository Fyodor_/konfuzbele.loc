<?php echo $header; ?>
<div id="container" class="container j-container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php echo $breadcrumb['href']; ?>" itemprop="url"><span itemprop="title"><?php echo $breadcrumb['text']; ?></span></a></li>
    <?php } ?>
  </ul>
  <?php if ($success) { ?>
  <div class="alert alert-success success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?><?php echo $column_right; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?> compare">
      <h1 class="heading-title"><?php echo $heading_title; ?></h1>
      <?php echo $content_top; ?>
      <?php if ($products) { ?>
        <div class="table-responsive">
      <table class="table table-bordered compare-info">
        <tbody>
          <tr>
            <td><?php //echo $text_image; ?></td>
            <?php foreach ($products as $product) { ?>
				<td class="text-center image-compare">
					<?php if ($products[$product['product_id']]['thumb']) { ?>
						<img src="<?php echo $products[$product['product_id']]['thumb']; ?>" alt="<?php echo $products[$product['product_id']]['name']; ?>" title="<?php echo $products[$product['product_id']]['name']; ?>" class="img-thumbnail" />
					<?php } ?>
					<a href="<?php echo $product['remove']; ?>" class="btn-danger btn-block button compare-remove"><?php echo $button_remove; ?></a>
				</td>
            <?php } ?>
          </tr>
		  <tr>
            <td><?php //echo $text_name; ?></td>
            <?php foreach ($products as $product) { ?>
            <td class="name"><a href="<?php echo $products[$product['product_id']]['href']; ?>"><?php echo $products[$product['product_id']]['name']; ?></a></td>
            <?php } ?>
          </tr>
          <tr>
            <td><?php //echo $text_price; ?></td>
            <?php foreach ($products as $product) { ?>
            <td><?php if ($products[$product['product_id']]['price']) { ?>
              <?php if (!$products[$product['product_id']]['special']) { ?>
              <span class="price-new"><?php echo $products[$product['product_id']]['price']; ?></span>
              <?php } else { ?>
              <span class="price-old"><?php echo $products[$product['product_id']]['price']; ?> </span> <span class="price-new"> <?php echo $products[$product['product_id']]['special']; ?> </span>
              <?php } ?>
              <?php } ?></td>
            <?php } ?>
          </tr>
		  <tr>
			  <td><?php //echo $text_price; ?></td>
			  <?php foreach ($products as $product) { ?>
				  <td>
					<?php if (Journal2Utils::isEnquiryProduct($this, $product)): ?>
					<div class="cart enquiry-button">
					  <a href="javascript:Journal.openPopup('<?php echo $this->journal2->settings->get('enquiry_popup_code'); ?>', '<?php echo $product['product_id']; ?>');" data-clk="addToCart('<?php echo $product['product_id']; ?>');" class="button hint--top compare-enquire" data-hint="<?php echo $this->journal2->settings->get('enquiry_button_text'); ?>"><?php echo $this->journal2->settings->get('enquiry_button_icon') . '<span class="button-cart-text">В корзину</span>'; ?></a>
						<a href="<?php echo $product['remove']; ?>" class="btn-danger btn-block button compare-remove"><?php echo $button_remove; ?></a>
					</div>
					<?php else: ?>
					<div class="cart <?php echo isset($product['labels']) && is_array($product['labels']) && isset($product['labels']['outofstock']) ? 'outofstock' : ''; ?>">
					  <a onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button hint--top compare-add-to-cart" data-hint="<?php echo $button_cart; ?>"><i class="button-left-icon"></i><span class="button-cart-text">В корзину</span><i class="button-right-icon"></i></a>
					</div>
					<?php endif; ?>
				  </td>
			  <?php } ?>
		  </tr>	
        </tbody>
        <?php foreach ($attribute_groups as $attribute_group) { ?>
        <?php foreach ($attribute_group['attribute'] as $key => $attribute) { ?>
        <tbody class="attr-compare">
          <tr>
            <td style="text-align:left;"><?php echo $attribute['name']; ?></td>
            <?php foreach ($products as $product) { ?>
				<?php if (isset($products[$product['product_id']]['attribute'][$key])) { ?>
					<td><?php echo $products[$product['product_id']]['attribute'][$key]; ?></td>
				<?php } else { ?>
            <td></td>
            <?php } ?>
            <?php } ?>
          </tr>
        </tbody>
        <?php } ?>
        <?php } ?>
      </table>
        </div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-default button"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    </div>
</div>
<?php echo $footer; ?>
