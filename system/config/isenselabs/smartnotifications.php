<?php

$_['smartnotifications_name'] 							= 'SmartNotifications';
$_['smartnotifications_name_small'] 					= 'smartnotifications';

$_['smartnotifications_version'] 						= '2.2.6';

$_['smartnotifications_path'] 							= 'extension/module/smartnotifications';
$_['smartnotifications_model_call']						= 'model_extension_module_smartnotifications';

$_['smartnotifications_extensions_link']				= 'extension/extension';
$_['smartnotifications_extensions_link_params']			= '&type=module';