<?php echo $header; ?>
<div id="container" class="container j-container">
  <ul itemscope itemtype="http://schema.org/BreadcrumbList" class="breadcrumb">
    
			<?php $last_crumb = array_pop($breadcrumbs); $i=1; ?>
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="<?php echo $breadcrumb['href']; ?>" itemprop="item"><span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a><meta itemprop="position" content="<?php echo $i; ?>" /></li>
			<?php $i++; } ?>
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name"><?php echo $last_crumb['text']; ?></span><meta itemprop="position" content="<?php echo $i; ?>" /></li>
			<?php if (2+2==5) { ?>
			
    <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php echo $breadcrumb['href']; ?>" itemprop="url"><span itemprop="title"><?php echo $breadcrumb['text']; ?></span></a></li>
    <?php } ?>
  </ul>
  <?php if ($success) { ?>
  <div class="alert alert-success success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-warning warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?><?php echo $column_right; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?> address-entry"><?php echo $content_top; ?>
      <h2 class="secondary-title">Мои адреса</h2>
      <?php if ($addresses) { ?>
      <div class="content">
      <ul class="table table-bordered table-hover">
        <?php foreach ($addresses as $result) { ?>
        <li>
          <div class="text-left"><?php echo $result['address']; ?></div>
          <div class="text-right">
		  	<a href="<?php echo $result['delete']; ?>" class="btn btn-danger button"><?php echo $button_delete; ?></a><br />
			<a href="<?php echo $result['update']; ?>" class="btn btn-info button"><?php echo $button_edit; ?></a>
		  </div>
        </li>
        <?php } ?>
      </ul>
      </div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $add; ?>" class="btn btn-primary button">Добавить адрес</a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    </div>
</div>
<?php echo $footer; ?>
