<?php echo $header; ?>
<div id="container" class="container j-container">
  <ul itemscope itemtype="http://schema.org/BreadcrumbList" class="breadcrumb">
    
			<?php $last_crumb = array_pop($breadcrumbs); $i=1; ?>
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="<?php echo $breadcrumb['href']; ?>" itemprop="item"><span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a><meta itemprop="position" content="<?php echo $i; ?>" /></li>
			<?php $i++; } ?>
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name"><?php echo $last_crumb['text']; ?></span><meta itemprop="position" content="<?php echo $i; ?>" /></li>
			<?php if (2+2==5) { ?>
			
    <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php echo $breadcrumb['href']; ?>" itemprop="url"><span itemprop="title"><?php echo $breadcrumb['text']; ?></span></a></li>
    <?php } ?>
  </ul>
  <?php if ($success) { ?>
  <div class="alert alert-success success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="row">
    <div id="content" class="col-sm-12">
      <?php echo $content_top; ?>
	  <div class="heading-title">Вход в систему</div>
      <div class="row login-content">
        <div class="col-sm-12 right">
          <div class="well">
				<p>Если Вы еще не зарегистрированы, перейдите на страницу <a href="/simpleregister">регистрации.</a></p>
				<p><?php echo $text_register_account; ?></p>
            <div class="tabbed-login">
				<span>Вход</span><a href="/simpleregister">Регистрация</a>
			</div>
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
			 <div class="login-wrap">
				  <div class="form-group">
					<label class="control-label" for="input-email">Введите Email<b>*</b></label>
					<input type="text" name="email" value="<?php echo $email; ?>" id="input-email" class="form-control" />
				  </div>
				  <div class="form-group">
					<label class="control-label" for="input-password">Введите пароль<b>*</b></label>
					<input type="password" name="password" value="<?php echo $password; ?>" id="input-password" class="form-control" />
					<div class="align-right">
						<a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?><b>*</b></a><br />
						<input type="submit" value="<?php echo $button_login; ?>" class="btn btn-primary button" />
					</div>
				  </div>	
			  </div>
              <?php if ($redirect) { ?>
              <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
              <?php } ?>
            </form>
			
          </div>
        </div>
      </div>
      <?php echo $content_bottom; ?></div>
    </div>
</div>
<?php echo $footer; ?>
